#!/usr/bin/env bash
IFS=$'\n'
tmp_file=`mktemp`
for i in `find . -type f -name 'uk.js'`
do
	cat $i | grep ": \"" | cut -f 2 -d ":" | cut -f 2 -d "\"" >>$tmp_file
done
aspell -l uk -c $tmp_file
rm $tmp_file

