#!/usr/bin/env bash
zenity --question --title="Гібернація" --text="Зберегти вміст пам’яті на swap-розділ і вимкнути комп’ютер?"
if [ $? == 0 ]
then
	echo Hibernating...
	slimlock &
	sudo pm-hibernate
else
	echo No action
fi

