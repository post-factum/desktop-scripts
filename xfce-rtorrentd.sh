#!/usr/bin/env bash

delay="1000"
pid1=`pidof rtorrent`

if [[ $pid1 != "" ]]
then
	notify-send -t $delay "Зупиняю rtorrentd…"
	sudo rc.d stop rtorrentd
	notify-send -t $delay "rtorrentd зупинено"
else
	notify-send -t $delay "Запускаю rtorrentd…"
	sudo rc.d start rtorrentd
	notify-send -t $delay "rtorrentd запущено"
fi

