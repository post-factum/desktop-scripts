#!/bin/bash
enca -x utf-8 "$1" ; \
	shnsplit -o flac "$2" < "$1" && \
	rm -f split-track00.flac ; \
	cuetag.sh "$1" split*.flac && \
	find . -maxdepth 1 -name "split*.flac" -print0 | xargs -0 metaflac --add-replay-gain && \
	lltag --yes --no-tagging --rename "%n - %t" split*.flac

