#!/usr/bin/env bash

delay="1000"
pid1=`pidof hostapd`

if [[ $pid1 != "" ]]
then
	notify-send -t $delay "Зупиняю точку доступу Wi-Fi…"
	sudo su -c "echo 0 >/proc/sys/net/ipv4/ip_forward"
	sudo iptables -t nat -F
	sudo iptables -t mangle -F
	sudo rc.d stop dhcp4
	sudo rc.d stop hostapd
	notify-send -t $delay "Точку доступу Wi-Fi зупинено"
else
	notify-send -t $delay "Запускаю точку доступу Wi-Fi…"
	sudo rmmod b43
	sudo modprobe b43
	sudo rc.d start hostapd
	sudo ifconfig wlan0 172.17.29.1 netmask 255.255.255.0
	sudo rc.d start dhcp4
	sudo su -c "echo 1 >/proc/sys/net/ipv4/ip_forward"
	#sudo iptables -t nat -A POSTROUTING -o ppp0 -j SNAT --to 77.47.181.230 -s 172.17.29.0/24
	sudo iptables -t nat -A POSTROUTING -o eth0 -j SNAT --to 192.168.1.3 -s 172.17.29.0/24
	sudo iptables -t mangle -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
	notify-send -t $delay "Точку доступу Wi-Fi запущено"
fi

