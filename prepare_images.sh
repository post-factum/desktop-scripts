#!/usr/bin/env bash

# create upload directory
tmpdir=`mktemp -d --tmpdir=.`

for i in *.jpg
do
	# resize image
	convert -resize 1366x768 -auto-orient "$i" $tmpdir/"_$i"

	# get image dimensions
	dimensions=`identify $tmpdir/"_$i" | awk '{print($3)}'`
	dim_x=`echo $dimensions | cut -d x -f 1`
	dim_y=`echo $dimensions | cut -d x -f 2`

	# get rectangle position
	top_x=`echo "$dim_x-320" | bc -l`
	top_y=`echo "$dim_y-16" | bc -l`

	# put copyright
	convert -pointsize 10 -font /usr/share/fonts/liberation/LiberationSans-Regular.ttf -draw "fill rgb(220,220,220) stroke rgb(220,220,220) rectangle $top_x,$top_y $dim_x,$dim_y" -draw 'gravity SouthEast fill black text 4,1 "© Oleksandr Natalenko :: https://natalenko.name/ :: CC BY-NC-SA 4.0"' $tmpdir/"_$i" $tmpdir/"__$i"

	# remove garbage
	rm $tmpdir/"_$i"
done

