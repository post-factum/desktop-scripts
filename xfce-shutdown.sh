#!/usr/bin/env bash
zenity --question --title="Вимкнення комп’ютера" --text="Зберегти сеанс і вимкнути комп’ютер?"
if [ $? == 0 ]
then
	echo Shutting down...
	xfce4-session-logout --halt
else
	echo No action
fi

