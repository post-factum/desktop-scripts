#!/usr/bin/env bash

if [[ "$#" -ne "1" ]]; then
	exit 1
fi

DEV=${1}

function now() {
	date +"%s %N" | awk '{print($1 * 1000000000 + $2)}'
}

function callback() {
	nice -n 19 ionice -c 3 taskset -c 0 dd if=${DEV} of=/dev/null iflag=direct,sync,noatime bs=4M count=1 status=none
}

while true; do
	start_time=$(now)
	while true; do
		callback
		now_time=$(now)
		if (( now_time - start_time >= 1000000000 )); then
			break
		fi
	done
	sleep 1
done
