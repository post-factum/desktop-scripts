#!/bin/bash
path="/media/sda5/Музыка"

find $path -regex ^.*\.[mp3\|ogg\|wma]$ | sed 's/ /%_/g' > /tmp/file.tmp

for file in `cat /tmp/file.tmp`;
do
    file=`echo $file | sed 's/%_/ /g'`
    echo $file
    mid3iconv -e cp1251 --remove-v1 "$file"
done