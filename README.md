desktop-scripts
===============

Stupid collection of simple and silly scripts, used by me to make life in desktop environment easier.

Scripts
-------

### any2flac.sh

Splits bloated all-in-one music files with cue sheets provided into separate flacs with tags preserving.

### cli-switch-openvpn-route

This crap switches default route between VPN tunnel and local gateway.

### conky-wrapper.sh

Helps to avoid multiple conky instances running.

### getlor

Gets funny phrases from Linux.Org.Ru and puts them into fortune DB.

### keyboard-layouts.sh

Enables 3 keyboard layouts for Ukrainian users with Dell laptops that respect typography.

### prepare\_images.sh

Resizes images and puts my copyright on them before publishing.

### rtorrentd

Arch Linux rc.d script to start rtorrent in background.

### volume\_ctl

ALSA utils wrapper to control the volume. Uses volnoti for OSD.

### Xfce session scripts

* xfce-fast-shutdown.sh — performs fast Xfce shutdown;
* xfce-lockns2disk.sh — locks screen with slimlock and hibernates;
* xfce-lockns2ram.sh — locks screen with slimlock and suspends;
* xfce-reboot.sh — reboots the machine;
* xfce-shutdown.sh — shuts machine down properly.

### xfce-rtorrentd.sh

GUI wrapper for rtorrentd script with notification.

### xfce-wifi-ap.sh

Nice wrapper to start/stop Wi-Fi access point.

Staging
-------

Scripts in **staging** folder are untested.

Distribution and Contribution
-----------------------------

These scripts are provided AS IS and are considered to be public domain. You may do with them what you want. You may kill your computer or other people with them. You may send me pull requests. But if you just don't like them — you may fuck off.
