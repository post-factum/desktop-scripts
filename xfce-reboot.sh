#!/usr/bin/env bash
zenity --question --title="Перезавантаження комп’ютера" --text="Зберегти сеанс і перезавантажити комп’ютер?"
if [ $? == 0 ]
then
	echo Rebooting...
	xfce4-session-logout --reboot
else
	echo No action
fi

