#!/usr/bin/env bash

for i in src/*.mp4; do
	output=dst/$(basename ${i})
	ab-av1 auto-encode --input "${i}" --output "${output}" --encoder libx265 --acodec libmp3lame --max-encoded-percent 110 --min-vmaf 93
	if [[ "${?}" == "0" ]]; then
		mv "${i}" ok
	else
		mv "${i}" notok
	fi
done
