<?php

function decodePDU($in) {
  $b = 0; $d = 0;
  $out = "";
  foreach (str_split($in, 2) as $ss) {
    $byte = hexdec($ss);
    $c = (($byte & ((1 << 7-$d)-1)) << $d) | $b;
    $b = $byte >> (7-$d);
    $out .= chr($c);
    $d++;
    if ($d == 7) {
      $out .= chr($b);
      $d = 0; $b = 0;
    }
  }
  return $out;
}

function encodePDU($in) {
  $out = "";
  for ($i = 0; $i < strlen($in) - 1; $i++) {
    $t = $i%8+1;
    if ($t == 8) 
      continue;
    $c = ord($in[$i])>>($i%8);
    $oc = $c;
    $b = ord($in[$i+1]) & ((1 << $t)-1);
    $c = ($b << (8-$t)) | $c;
    $out .= strtoupper(str_pad(dechex($c), 2, '0', STR_PAD_LEFT));
  }
  return $out;
}

$f = fopen ("/dev/ttyUSB2", "rw+");
fputs($f, "AT+CUSD=1,".encodePDU("*100#").",15\r\n");

while ($s = fgets($f)) {
  if (substr($s, 0, 5) == "+CUSD") {
    $s = decodePDU(substr(trim($s), 10, -3));
    echo $s."\n";
    if (strpos($s, "1 - Dalee") !== false) {
      fputs($f, "AT+CUSD=1,".encodePDU("1").",15\r\n");
    }
    else
      break;
  }
 }

fclose($f);

?>
