#!/usr/bin/env bash
zenity --question --title="Завершення сеансу" --text="Зберегти сеанс і завершити його?"
if [ $? == 0 ]
then
	echo Rebooting...
	xfce4-session-logout --logout
else
	echo No action
fi

