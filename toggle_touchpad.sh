#!/usr/bin/env bash

DEVICE="AlpsPS/2 ALPS GlidePoint"
LEDF="/sys/class/leds/dell-laptop::touchpad/brightness"
STATEF=$(realpath ~/.local/touchpad)

if [[ "${#}" != "1" ]]; then
	exit 1
fi

if [[ "${1}" == "load" ]]; then
	STATE=$(cat "${STATEF}")
	if [[ "${STATE}" == "0" ]]; then
		STATE="1"
	else
		STATE="0"
	fi
elif [[ "${1}" == "toggle" ]]; then
	STATE=$(xinput list-props "${DEVICE}" | awk '/Device Enabled/ { print($4) }')
else
	exit 2
fi

if [[ "${STATE}" == "0" ]]; then
	xinput enable "${DEVICE}"
	echo "255" >"${LEDF}"
	echo "1" >"${STATEF}"
else
	xinput disable "${DEVICE}"
	echo "0" >"${LEDF}"
	echo "0" >"${STATEF}"
fi
