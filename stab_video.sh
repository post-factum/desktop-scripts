#!/usr/bin/env bash

ffmpeg -i "${1}" -filter:v fps=film,vidstabdetect -f null -
ab-av1 auto-encode --input "${1}" --output "${2}" --acodec libopus --vfilter fps=film,vidstabtransform,unsharp=la=0.5 --preset 6

rm -f transforms.trf
