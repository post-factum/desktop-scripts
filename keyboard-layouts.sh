#!/usr/bin/env bash

setxkbmap -model dell -layout us,ru,ua -variant intl-unicode,winkeys,unicode -option grp:rctrl_toggle,lv3:lwin_switch,misc:typo,compose:ralt
