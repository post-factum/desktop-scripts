#!/usr/bin/env bash

while true
do
	pids=`pgrep conky`
	if [[ $pids == "" ]]
	then
		conky &
	else
		((index=0))
		for i in $pids
		do
			((index++))
			if [[ $index -gt 1 ]]
			then
				kill -s TERM $i
			fi
		done
	fi
	sleep 5

done

