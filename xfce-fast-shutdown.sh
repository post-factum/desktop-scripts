#!/usr/bin/env bash
zenity --question --title="Швидке вимкнення комп’ютера" --text="Вимкнути комп’ютер без збереження сеансу?"
if [ $? == 0 ]
then
	echo Shutting down quickly...
	xfce4-session-logout --fast
else
	echo No action
fi

