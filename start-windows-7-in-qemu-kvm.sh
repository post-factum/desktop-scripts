#!/usr/bin/env bash

MONITOR_HOST="127.0.0.1"
MONITOR_PORT="44001"
SPICE_HOST="127.0.0.1"
SPICE_PORT="5930"
VM_NAME="Windows 7"
CPU_COUNT="2"
RAM_COUNT="4096"
DRIVE="Windows 7.qcow2"
NET_INTERFACE="tap0"
MAC_ADDRESS="52:52:52:ac:3f:65"

export QEMU_AUDIO_DRV=alsa

nohup qemu-system-x86_64 \
	-name "$VM_NAME" \
	-cpu host \
	-enable-kvm \
	-smp $CPU_COUNT \
	-m $RAM_COUNT \
	-drive file="$DRIVE",media=disk,if=virtio \
	-vga qxl \
	-spice port=$SPICE_PORT,disable-ticketing \
	-device virtio-serial-pci \
	-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
	-chardev spicevmc,id=spicechannel0,name=vdagent \
	-rtc base=localtime \
	-soundhw ac97 \
	-usb \
	-usbdevice tablet \
	-net nic,model=virtio,macaddr=$MAC_ADDRESS \
	-net tap,ifname="$NET_INTERFACE",script=no,downscript=no \
	-monitor telnet:$MONITOR_HOST:$MONITOR_PORT,server,nowait \
	&
sleep 1
nohup spicec --host $SPICE_HOST --port $SPICE_PORT --title "$VM_NAME" --full-screen=auto-conf &

