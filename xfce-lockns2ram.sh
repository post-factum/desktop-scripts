#!/usr/bin/env bash
zenity --question --title="Призупинення" --text="Призупинити роботу комп’ютера?"
if [ $? == 0 ]
then
	echo Entering sleep state...
	slimlock &
	sudo pm-suspend
else
	echo No action
fi

